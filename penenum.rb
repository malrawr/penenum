#!/usr/bin/ruby

require 'fileutils'

# Helper methods are defined here
#module TerminalColor    
    RS = "\033[0m"    # RESET
    HC = "\033[1m"    # Strong BOLD
    FBLU = "\033[34m" # Foreground Blue
    FRED = "\033[31m" # Foreground Red
    FGRN = "\033[32m" # Foreground Green
    FWHT = "\033[37m" # Foreground White

    def print_stat(string)
        puts "#{FBLU}#{HC}[*]#{HC}#{FBLU}" << print_reset << " " << string
    end

    def print_warn(string)
        puts "#{FRED}#{HC}[x]#{HC}#{FRED}" << print_reset << " " << string
    end

    def print_sugg(string)
        puts "   #{FGRN}#{HC}[>]#{HC}#{FGRN}" << print_reset << " " << string
    end

    def print_exec(string)
        $GLOBAL_ARRAY << string
        puts "      #{FWHT}#{HC}[#{$GLOBAL_ARRAY.index(string)}]#{HC}#{FWHT}" << print_reset << " " << string
    end

    def print_norm(string)
        puts "      #{FWHT}#{HC}[=]#{HC}#{FWHT}" << print_reset << " " << string
    end

    def print_exit(string)
        puts ""
        puts "#{FRED}" << string << "#{FRED}" << print_reset
        puts ""
    end

    def print_reset
        return "#{RS}"
    end

    def prompt(string, warn=false)
        if warn
            print "\n#{FRED}#{HC}>>#{FRED}#{HC}" << " " << print_reset <<  string << "\n" << "#{HC}>?#{HC}" << print_reset << " "
        else
            print "\n#{FGRN}#{HC}>>#{FGRN}#{HC}" << " " << print_reset <<  string << "\n" << "#{HC}>?#{HC}" << print_reset << " " 
        end
    end

    def space
        puts ""
    end

#end
# End helper methods

def banner; end       

def nmap_tcp_exec(fulltest = false)
    print_stat "Running NMAP on #{$IP_ADDRESS} to find OPEN Ports and Services"
    
    if fulltest
        print_sugg "Executing Full TCP Scan"
        print_norm "nmap -v -Pn -A -sC -sS -T 4 #{$IP_ADDRESS} -p- -oG #{$FILE_OUTPUT}/greplist_#{$IP_ADDRESS}.nmap -oN #{$FILE_OUTPUT}/tcp_full_#{$IP_ADDRESS}.nmap"
        system("bash", "-c", "nmap -v -Pn -A -sC -sS -T 4  #{$IP_ADDRESS} -p- -oG #{$FILE_OUTPUT}/greplist_#{$IP_ADDRESS}.nmap -oN #{$FILE_OUTPUT}/tcp_full_#{$IP_ADDRESS}.nmap")
        space
    else
        print_sugg "Executing Standard TCP Scan"        
        print_norm "nmap -sC -sV #{$IP_ADDRESS} -oG #{$FILE_OUTPUT}/greplist_#{$IP_ADDRESS}.nmap -oN #{$FILE_OUTPUT}/tcp_standard_#{$IP_ADDRESS}.nmap"
        system("bash", "-c", "nmap -sC -sV #{$IP_ADDRESS} -oG #{$FILE_OUTPUT}/greplist_#{$IP_ADDRESS}.nmap -oN #{$FILE_OUTPUT}/tcp_standard_#{$IP_ADDRESS}.nmap")
        space
    end
end

def nmap_udp_exec
    print_sugg "Executing Top 200 UDP Scan"
    #Process.spawn "nmap -v -Pn -A -sC -sU -T 4 #{$IP_ADDRESS} --top-ports 200 -oN #{$FILE_OUTPUT}/udp_top200_#{$IP_ADDRESS}.nmap"
    Process.fork { system("bash", "-c", "nmap -v -Pn -A -sC -sU -T 4 #{$IP_ADDRESS} --top-ports 200 -oN #{$FILE_OUTPUT}/udp_top200_#{$IP_ADDRESS}.nmap", :out => File::NULL) }
end

def parse_nmap
    # Go back and fix this line below
    #test_line = IO.readlines("grep.config")[4].gsub(/\s+/, "").tr(',', "\n").sub(/Ignored.*/mi, "").gsub(/.*:/, '')
    `cat #{$FILE_OUTPUT}/greplist_#{$IP_ADDRESS}.nmap | grep "Ports:" | sed 's/Ignored.*//' | cut -d " " -f4- | tr "," "\n" | tr -d " " | tee #{$FILE_OUTPUT}/parsed_#{$IP_ADDRESS}.list &> /dev/null`
    `cat #{$FILE_OUTPUT}/parsed_#{$IP_ADDRESS}.list | cut -d "/" -f1 | tee #{$FILE_OUTPUT}/ports_#{$IP_ADDRESS}.list &> /dev/null`
    `cat #{$FILE_OUTPUT}/parsed_#{$IP_ADDRESS}.list | cut -d "/" -f5 | tee #{$FILE_OUTPUT}/services_#{$IP_ADDRESS}.list &> /dev/null`
end

def port_list
    port_list_path = File.join($FILE_OUTPUT, "ports_#{$IP_ADDRESS}.list")
    port_list_file = File.open(port_list_path, 'r').readlines
    port_list_file.map! { |x| x.chomp }
    return port_list_file
end

def service_list
    service_list_path = File.join($FILE_OUTPUT, "services_#{$IP_ADDRESS}.list")
    service_list_file = File.open(service_list_path, 'r').readlines
    service_list_file.map! { |x| x.chomp }    
    return service_list_file
end

def command_suggestions(ports, services)
    ports.zip(services) do | port, service |
        if service == "" || service == "unknown"
            $UNKNOWN_ARRAY << "Found an unknown service on #{$IP_ADDRESS}:#{port}"
            #print_warn "Found an unknown service on #{$IP_ADDRESS}:#{port}"
            #print_sugg "Try using CURL or AMAP to see what it might be"
            #print_exec "curl #{$IP_ADDRESS}:#{port}"
            #print_exec "amap -d #{$IP_ADDRESS} #{port}"
            #space

        elsif service == "ftp" || service == "tftp"
            print_stat "Found FTP service on #{$IP_ADDRESS}:#{port}"
            print_sugg "Suggested Commands:"
            print_exec "nmap -n -Pn -sV #{$IP_ADDRESS} -p #{port} --script=ftp-anon,ftp-bounce,ftp-libopie,ftp-proftpd-backdoor,ftp-vsftpd-backdoor,ftp-vuln-cve2010-4221 -oN #{$FILE_OUTPUT}/ftp_#{$IP_ADDRESS}-#{port}.nmap"
            print_exec "hydra -L /usr/share/metasploit-framework/data/wordlists/unix_users.txt -P /usr/share/metasploit-framework/data/wordlists/unix_passwords.txt -f -o #{$FILE_OUTPUT}/ftphydra_#{$IP_ADDRESS}-#{port} -u #{$IP_ADDRESS} -s #{port} ftp"
            space

         elsif service == "ssh"
            print_stat "Found SSH service on #{$IP_ADDRESS}:#{port}"
            print_sugg "Suggested Commands:"
            print_norm "medusa -u root -P /usr/share/wordlists/rockyou.txt -e ns -h #{$IP_ADDRESS} - #{port} -M ssh -f"
            print_norm "medusa -U /usr/share/metasploit-framework/data/wordlists/unix_users.txt -P/usr/share/metasploit-framework/data/wordlists/unix_passwords.txt -e ns -h #{$IP_ADDRESS} - #{port} -M ssh -f"
            print_norm "hydra -f -V -t 1 -l root -P /usr/share/wordlists/rockyou.txt -s #{port} #{$IP_ADDRESS} ssh"
            space

        elsif service == "smtp"
            print_stat "Found SMTP service on #{$IP_ADDRESS}:#{port}"
            print_sugg "Suggested Commands:"
            print_exec "nmap -n -Pn -sV #{$IP_ADDRESS} -p #{port} --script=smtp* -oN #{$FILE_OUTPUT}/smtp_#{$IP_ADDRESS}-#{port}.nmap"
            print_exec "smtp-user-enum -M VRFY -U /usr/share/metasploit-framework/data/wordlists/unix_users.txt -t #{$IP_ADDRESS} -p #{port} | tee #{$FILE_OUTPUT}/smtp_enum_#{$IP_ADDRESS}-#{port}"
            space

        elsif service == "snmp"
            print_stat "Found SNMP service on #{$IP_ADDRESS}:#{port}"
            print_sugg "Suggested Commands:"
            print_exec "nmap -n -Pn -sV #{$IP_ADDRESS} -p #{$IP_ADDRESS} --script=snmp-netstat,snmp-processes -oN #{$FILE_OUTPUT}/#{$IP_ADDRESS}:#{port}_snmp.nmap"
            print_exec "onesixtyone -c public #{$IP_ADDRESS} | tee #{$FILE_OUTPUT}/161_#{$IP_ADDRESS}-#{port}"
            print_exec "snmpwalk -c public -v1 #{$IP_ADDRESS} | tee #{$FILE_OUTPUT}/snmpwalk_#{$IP_ADDRESS}-#{port}"
            print_exec "snmpwalk -c public -v1 #{$IP_ADDRESS} 1.3.6.1.4.1.77.1.2.25 | tee #{$FILE_OUTPUT}/snmp_users_#{$IP_ADDRESS}-#{port}"
            print_exec "snmpwalk -c public -v1 #{$IP_ADDRESS} 1.3.6.1.2.1.6.13.1.3 | tee #{$FILE_OUTPUT}/snmp_ports_#{$IP_ADDRESS}-#{port}"
            print_exec "snmpwalk -c public -v1 #{$IP_ADDRESS} 1.3.6.1.2.1.25.4.2.1.2 | tee #{$FILE_OUTPUT}/snmp_process_#{$IP_ADDRESS}-#{port}"
            print_exec "snmpwalk -c public -v1 #{$IP_ADDRESS} 1.3.6.1.2.1.25.6.3.1.2 | tee #{$FILE_OUTPUT}/snmp_software_#{$IP_ADDRESS}-#{port}"
            space

         elsif service == "http" || service == "www" || service == "http-alt" || service == "http-alt-alt" || service == "www-http" || service == "www-dev"
            print_stat "Found HTTP service on #{$IP_ADDRESS}:#{port}"
            print_sugg "Suggested Commands:"
            print_exec "nmap -n -Pn -sV #{$IP_ADDRESS} -p #{port} --script=http-enum,http-userdir-enum,http-apache-negotiation,http-backup-finder,http-config-backup,http-default-accounts,http-methods,http-method-tamper,http-passwd,http-robots.txt,http-iis-webdav-vuln,http-vuln-cve2009-3960,http-vuln-cve2010-0738,http-vuln-cve2011-3368,http-vuln-cve2012-1823,http-vuln-cve2013-0156,http-waf-detect,http-waf-fingerprint,ssl-enum-ciphers,ssl-known-key -oN #{$FILE_OUTPUT}/http_#{$IP_ADDRESS}-#{port}.nmap"
            print_exec "nikto -h #{$IP_ADDRESS} -p #{port} | tee #{$FILE_OUTPUT}/nikto_#{$IP_ADDRESS}-#{port}"
            print_exec "gobuster -u http://#{$IP_ADDRESS}:#{port}/ -w /usr/share/seclists/Discovery/Web_Content/cgis.txt -s '200,204,301,307,403,500' -e | tee #{$FILE_OUTPUT}/gobuster_cgis_#{$IP_ADDRESS}-#{port}"
            print_exec "gobuster -u http://#{$IP_ADDRESS}:#{port}/ -w /usr/share/seclists/Discovery/Web_Content/common.txt -s '200,204,301,307,403,500' -e | tee #{$FILE_OUTPUT}/gobuster_common_#{$IP_ADDRESS}-#{port}"
            space

        elsif service == "ssl/http" || service == "https" || service == "ssl|http"
            print_stat "Found HTTPS service on #{$IP_ADDRESS}:#{port}"
            print_sugg "Suggested Commands:"
            print_exec "nmap -n -Pn -sV #{$IP_ADDRESS} -p #{port} --script=ssl-heartbleed,http-enum,http-userdir-enum,http-apache-negotiation,http-backup-finder,http-config-backup,http-default-accounts,http-methods,http-method-tamper,http-passwd,http-robots.txt,http-iis-webdav-vuln,http-vuln-cve2009-3960,http-vuln-cve2010-0738,http-vuln-cve2011-3368,http-vuln-cve2012-1823,http-vuln-cve2013-0156,http-waf-detect,http-waf-fingerprint,ssl-enum-ciphers,ssl-known-key -oN #{$FILE_OUTPUT}/https_#{$IP_ADDRESS}-#{port}.nmap"
            print_exec "nikto -h #{$IP_ADDRESS} -p #{port} | tee #{$FILE_OUTPUT}/nikto_#{$IP_ADDRESS}-#{port}"
            print_exec "gobuster -u https://#{$IP_ADDRESS}:#{port}/ -w /usr/share/seclists/Discovery/Web_Content/cgis.txt -s '200,204,301,307,403,500' -e | tee #{$FILE_OUTPUT}/gobuster_cgis_#{$IP_ADDRESS}-#{port}"
            print_exec "gobuster -u https://#{$IP_ADDRESS}:#{port}/ -w /usr/share/seclists/Discovery/Web_Content/common.txt -s '200,204,301,307,403,500' -e | tee #{$FILE_OUTPUT}/gobuster_common_#{$IP_ADDRESS}-#{port}"
            space

        elsif service == "microsoft-ds" || service == "netbios-ssn"
            print_stat "Found SMB service on #{$IP_ADDRESS}:#{port}"
            print_sugg "Suggested Commands:"
            print_exec "nmap -n -Pn -sV #{$IP_ADDRESS} -pT:139,#{port},U:137 --script=nbstat,smb-enum-domains,smb-enum-groups,smb-enum-processes,smb-enum-sessions,smb-ls,smb-mbenum,smb-os-discovery,smb-print-text,smb-security-mode,smb-server-stats,smb-system-info,smb-vuln-conficker,smb-vuln-ms06-025,smb-vuln-ms07-029,smb-vuln-ms08-067,smb-vuln-ms10-054,smb-vuln-ms10-061 -oN #{$FILE_OUTPUT}/smb_#{$IP_ADDRESS}-#{port}.nmap"
            print_exec "enum4linux #{$IP_ADDRESS} | tee #{$FILE_OUTPUT}/enum4linux_#{$IP_ADDRESS}-#{port}"
            print_exec "smbclient -L\\\\ -N -I #{$IP_ADDRESS} | tee #{$FILE_OUTPUT}/smbclient_#{$IP_ADDRESS}-#{port}"
            space

        elsif service == "msdrdp" || service == "ms-wbt-server"
            print_stat "Found RDP service on #{$IP_ADDRESS}:#{port}"
            print_sugg "Consider using a password attack on the target"
            print_norm "ncrack -vv --user Administrator -P /usr/share/wordlists/rockyou.txt rdp://#{$IP_ADDRESS}"
            space

        elsif service == "mysql"
            print_stat "Found MYSQL service on #{$IP_ADDRESS}:#{port}"
            print_sugg "Suggested Commands:"
            print_exec "nmap -n -Pn -sV #{$IP_ADDRESS} -p #{port} --script=mysql-audit,mysql-brute,mysql-databases,mysql-dump-hashes,mysql-empty-password,mysql-enum,mysql-info,mysql-query,mysql-users,mysql-variables,mysql-vuln-cve2012-2122 -oN #{$FILE_OUTPUT}/mysql_#{$IP_ADDRESS}-#{port}.nmap"  
            space

        elsif service == "ms-sql"
            print_stat "Found MSSQL service on #{$IP_ADDRESS}:#{port}"
            print_sugg "Suggested Commands:"
            print_exec "nmap -n -Pn -sV #{$IP_ADDRESS} -p #{port} --script=ms-sql-info,ms-sql-config,ms-sql-dump-hashes --script-args=mssql.instance-port=#{port},smsql.username-sa,mssql.password-sa -oN #{$FILE_OUTPUT}/mssql_#{$IP_ADDRESS}-#{port}.nmap"
            print_exec "nmap -n -Pn #{$IP_ADDRESS} -p #{port} --script ms-sql-xp-cmdshell --script-args mssql.username=sa,mssql.password=password,mssql.instance-port=#{port},ms-sql-xp-cmdshell.cmd='ipconfig' -oN #{$FILE_OUTPUT}/mssql_cmdshell_#{$IP_ADDRESS}-#{port}.nmap"
            space

        else
            #print_warn "Found #{service} service on #{$IP_ADDRESS}:#{port}"
            #print_sugg "Could not enumerate, look into it further"
            #space
            $UNKNOWN_ARRAY << "Found #{service} service on #{$IP_ADDRESS}:#{port}"

        end
    end

    $UNKNOWN_ARRAY.each do |x| 
        print_warn "#{x}"
    end
    print_sugg "Could not enumerate these services, look into them further with CURL or AMAP"
    print_norm "Example: curl #{$IP_ADDRESS}:80"
    print_norm "Example: amap -d #{$IP_ADDRESS} 80"

end

def command_execution
    # Make sure that people can't put in a value larger than array.length
    print_stat("What would you like to run? There are #{$GLOBAL_ARRAY.length} commands available. Add a space next to each selection.")
    print_stat("Example Selection: 2 1 7")
    value = gets.chomp.split(' ').map(&:to_i)
    value.each do | x |
        if x < $GLOBAL_ARRAY.length
            #Process.fork { system("#{$GLOBAL_ARRAY[x]}", "&") }
            Process.fork { system("bash", "-c", "#{$GLOBAL_ARRAY[x]}", :out => File::NULL) }
        else
            print_warn "An invalid number was included, ommiting #{$GLOBAL_ARRAY[x]}..."
        end
    end
    print_stat("All commands should be running now. Good luck! You can find that results at: #{$FILE_OUTPUT}")
end

$CURRENT_DIRECTORY = File.dirname(__FILE__)
$GLOBAL_ARRAY = []
$UNKNOWN_ARRAY = []
$IP_ADDRESS = "0"

trap("SIGINT") do
    print_exit "WARNING! CTRL + C Detected, Shutting things down and exiting program...."
    exit 666
end

ARGV << '--help' unless ARGV.include?('-h')
ARGV << '--help' if ARGV.empty?

if ARGV.include?('--help')
    puts "Usage: penenum.rb [options]
    -h, --target TARGET IP           Mandatory Target IP
    -t, --thorough                   TCP Scan All Ports
    -u, --udp                        Top 200 UDP Ports
    --help                           Displays Help"
    exit
end

if ARGV.include?('-h') && ARGV[ARGV.index('-h') + 1]
    puts "Target: #{ARGV[ARGV.index('-h') + 1]}"
    $IP_ADDRESS = "#{ARGV[ARGV.index('-h') + 1]}"    
end

$FILE_OUTPUT = "#{$CURRENT_DIRECTORY}" << "/results/" << "#{$IP_ADDRESS}"

print_stat "Preparing output directories, #{$FILE_OUTPUT}"
FileUtils.mkdir_p "#{$FILE_OUTPUT}"

if ARGV.include?('-t') || ARGV.include?('--thorough')
    puts "Running Full TCP Scan"
    nmap_tcp_exec(true)
else
    nmap_tcp_exec
end

if ARGV.include?('-u') || ARGV.include?('--udp')
    puts "Running Top 200 UDP"
    nmap_udp_exec
end

# Clears ARGV array that you can accept gets input
ARGV.clear

parse_nmap
command_suggestions(port_list, service_list)

prompt "Would you like to run any commands? y/n"
while user_input = gets.chomp.downcase
    case user_input
    when "y"
        command_execution
        break
    when "n"
        print_stat("Good luck!")
        break
    else
        prompt "Select a correct response.", true
    end
end
