# Penenum

## Issues

General Issues
* Need to change the script name from penenum to something else
* Needs a cool banner/logo

Ruby Version
* Refactoring needed
* Doesn't follow guidelines
* FIXED: Has a problem with child processes
    * FIXED: I have a possible fix soon, it's in the comments on the bottom of the file
* I don't believe that errors are thrown out of a child process fails, need to look into this further with more testing
    * I need to create a method that checks STDERR
* Reducing the amount of global arrays
	* The $UNKNOWN_ARRAY can be put within a method, however I was lazy
        * The $GLOBAL_ARRAY should be adjusted also

Bash Version
* Needs to be more tidy

Both Versions
* Commands need to be updated
    * Switch to default Kali tool commands and wordlists
    * Fix the bruteforcing commands so that they can effictively be used
        * Right now they don't output back to tee, consider changing this so that they don't become one of the recommended commands or fixing it

Future Features
* Implementing a config file so that commands and services can be grabbed from it instead of it being hardcoded within the code
* Use unicornscan to do the all port scan instead of nmap.
	* Unicornscan can do this faster, so have it do the scan first, then send the results over to nmap so that it can perform agressive, script, and service scanning.
